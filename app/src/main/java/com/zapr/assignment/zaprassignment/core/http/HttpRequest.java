package com.zapr.assignment.zaprassignment.core.http;

import com.zapr.assignment.zaprassignment.core.request.RequestData;
import com.zapr.assignment.zaprassignment.core.request.ResponseHandler;

public class HttpRequest {

    private RequestData mRequestData;
    private ResponseHandler mResponseHandler;

    public HttpRequest(RequestData requestData, ResponseHandler responseHandler) {
        mRequestData = requestData;
        mResponseHandler = responseHandler;
    }

    public RequestData getRequestData() {
        return mRequestData;
    }

    public ResponseHandler getResponseHandler() {
        return mResponseHandler;
    }
}
