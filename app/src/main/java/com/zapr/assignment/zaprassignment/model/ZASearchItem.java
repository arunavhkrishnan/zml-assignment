package com.zapr.assignment.zaprassignment.model;

import org.json.JSONException;
import org.json.JSONObject;

public class ZASearchItem extends ZACoreItem {

    private String mWrapperType;
    private String mKind;
    private String mTrackName;
    private String mArtistName;
    private String mThumbnail;

    public String getWrapperType() {
        return mWrapperType;
    }

    public String getKind() {
        return mKind;
    }

    public String getTrackName() {
        return mTrackName;
    }

    public String getArtistName() {
        return mArtistName;
    }

    public String getThumbnailUri() {
        return mThumbnail;
    }

    public static  ZASearchItem create(JSONObject jsonObject) {
        ZASearchItem item = null;
        try {
            item = new ZASearchItem();
            item.mWrapperType = jsonObject.getString(WRAPPER_TYPE);
            item.mKind = jsonObject.getString(KIND);
            item.mTrackName = jsonObject.getString(TRACK_NAME);
            item.mArtistName = jsonObject.getString(ARTIST_NAME);
            item.mThumbnail = jsonObject.getString(THUMBNAIL);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return item;
    }

    private final static String WRAPPER_TYPE = "wrapperType";
    private final static String KIND = "kind";
    private final static String TRACK_NAME = "trackName";
    private final static String ARTIST_NAME = "artistName";
    private final static String THUMBNAIL = "artworkUrl100";
}
