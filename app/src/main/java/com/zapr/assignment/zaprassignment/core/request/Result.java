package com.zapr.assignment.zaprassignment.core.request;

import org.json.JSONArray;
import org.json.JSONObject;

public class Result {

    public enum STATUS {
        SUCCESS, FAIL
    }

    private STATUS mStatus;
    private int mStatusCode;
    private JSONObject mJsonResult;
    private JSONArray mJsonArrayResult;
    private boolean mBooleanResult;
    private String mMessage;

    public Result(STATUS status, int statusCode, JSONObject jsonResult) {
        mStatus = status;
        mStatusCode = statusCode;
        mJsonResult = jsonResult;
    }

    public Result(STATUS status, int statusCode, JSONArray jsonArray) {
        mStatus = status;
        mStatusCode = statusCode;
        mJsonArrayResult = jsonArray;
    }

    public Result(STATUS status, int statusCode, boolean booleanResult) {
        mStatus = status;
        mStatusCode = statusCode;
        mBooleanResult = booleanResult;
    }

    public Result(STATUS status, int statusCode, String message) {
        mStatus = status;
        mStatusCode = statusCode;
        mMessage = message;
    }

    public STATUS getStatus() {
        return mStatus;
    }

    public int getStatusCode() {
        return mStatusCode;
    }

    public JSONObject getResultData() {
        return mJsonResult;
    }

    public boolean getBooleanResult() {
        return mBooleanResult;
    }

    public JSONArray getJsonArrayResult() {
        return mJsonArrayResult;
    }
}
