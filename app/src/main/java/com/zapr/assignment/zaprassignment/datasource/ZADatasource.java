package com.zapr.assignment.zaprassignment.datasource;

import com.zapr.assignment.zaprassignment.core.Constants;
import com.zapr.assignment.zaprassignment.core.http.HttpRequest;
import com.zapr.assignment.zaprassignment.core.request.RequestData;
import com.zapr.assignment.zaprassignment.core.request.ResponseHandler;
import com.zapr.assignment.zaprassignment.core.request.Result;
import com.zapr.assignment.zaprassignment.core.webservices.WebServiceClient;
import com.zapr.assignment.zaprassignment.model.ZACoreItem;
import com.zapr.assignment.zaprassignment.model.ZASearchItem;
import com.zapr.assignment.zaprassignment.managers.ConcurrencyManager;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by arunak on 12/11/17.
 */

public class ZADatasource {

    private static ZADatasource sZADatasource;

    private WebServiceClient mWebService;

    private ZADatasource() {
        init();
    }

    public static synchronized ZADatasource getInstance() {
        if (sZADatasource == null) {
            sZADatasource = new ZADatasource();
        }
        return sZADatasource;
    }

    private void init() {
        String basePath = Constants.HOST;
        if (!Constants.SUB_PATH.isEmpty()) {
            basePath = basePath + "/" + Constants.SUB_PATH;
        }
        mWebService = new WebServiceClient(basePath, Constants.PORT);
    }

    public void getSearchResults(String searchQuery, int limit, final ZAResultCallback callback) {
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("term", searchQuery);
        queryParams.put("limit", limit + "");
        RequestData requestData = new RequestData(UUID.randomUUID().toString(), GET_SEARCH_RESULTS, queryParams);
        HttpRequest request = new HttpRequest(requestData, new ResponseHandler() {
            @Override
            public void processResult(RequestData requestData, Result result) {
                if (result.getStatus().equals(Result.STATUS.SUCCESS)) {
                    final List<ZACoreItem> items = new ArrayList<>();
                    JSONArray array = result.getResultData().optJSONArray("results");
                    for (int i = 0; i < array.length(); i++) {
                        try {
                            items.add(ZASearchItem.create(array.getJSONObject(i)));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Runnable returnResults = new Runnable() {
                        @Override
                        public void run() {
                            callback.resultItems(items);
                        }
                    };
                    ConcurrencyManager.getInstance().runOnMainThread(returnResults);
                }
            }
        });
        mWebService.sendRequest(request);
    }

    public static final String GET_SEARCH_RESULTS = "search";

}
