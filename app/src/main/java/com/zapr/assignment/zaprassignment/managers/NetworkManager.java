package com.zapr.assignment.zaprassignment.managers;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.zapr.assignment.zaprassignment.core.receivers.NetworkBroadcastReceiver;

import java.util.ArrayList;
import java.util.List;

import static android.net.ConnectivityManager.CONNECTIVITY_ACTION;

public class NetworkManager {

    private Context mContext;
    private static NetworkManager sICSNetworkManager;
    private List<OnNetworkChangeListener> mNetworkChangeListeners;

    private NetworkManager(Context context) {
        mContext = context;
        mNetworkChangeListeners = new ArrayList<>();
        NetworkBroadcastReceiver receiver = new ZANetworkBroadcastReceiver();
        IntentFilter connectivityFilter = new IntentFilter();
        connectivityFilter.addAction(CONNECTIVITY_ACTION);
        context.registerReceiver(receiver, connectivityFilter);
    }

    public static synchronized void createInstance(Context context) {
        if (sICSNetworkManager == null) {
            sICSNetworkManager = new NetworkManager(context);
        }
    }

    public static synchronized NetworkManager getInstance() {
        return sICSNetworkManager;
    }

    //for direct queries on network state.
    public boolean isNetworkAvailable() {
        return isNetworkAvailable(mContext);
    }

    //for registering a listener for network change.
    public void registerNetworkChangeListener(OnNetworkChangeListener listener) {
        mNetworkChangeListeners.add(listener);
    }

    public void deregisterNetworkChangeListener(OnNetworkChangeListener listener) {
        mNetworkChangeListeners.remove(listener);
    }

    private void onNetworkChange() {
        for (OnNetworkChangeListener listener : mNetworkChangeListeners) {
            listener.onNetworkChange(isNetworkAvailable());
        }
    }

    private boolean isNetworkAvailable(Context context) {
        boolean isNetworkAvailable = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                isNetworkAvailable = true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                isNetworkAvailable = true;
            }
        } else {
            isNetworkAvailable = false;
        }
        return isNetworkAvailable;
    }

    public interface OnNetworkChangeListener {
        void onNetworkChange(boolean isOnline);
    }

    private class ZANetworkBroadcastReceiver extends NetworkBroadcastReceiver {

        @Override
        protected void onNetworkChange(Context context) {
            NetworkManager.this.onNetworkChange();
        }
    }
}
