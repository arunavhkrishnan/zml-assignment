package com.zapr.assignment.zaprassignment.datasource;

import com.zapr.assignment.zaprassignment.model.ZACoreItem;

import java.util.List;

public interface ZAResultCallback {
    void resultItems(List<ZACoreItem> resultItems);
}
