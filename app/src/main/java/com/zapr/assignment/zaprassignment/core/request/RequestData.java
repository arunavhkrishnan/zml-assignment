package com.zapr.assignment.zaprassignment.core.request;

import android.net.Uri;

import java.util.Map;

public class RequestData {

    private String mPath;
    private String mBasePath;
    private String mRequestId;

    public RequestData(String requestId, String path) {
        mRequestId = requestId;
        mPath = path;
    }

    public RequestData(String requestId, String path, String[] inlineParams) {
        mRequestId = requestId;
        mPath = String.format(path, (Object[]) inlineParams);
    }

    public RequestData(String requestId, String path, Map<String, String> queryParams) {
        mRequestId = requestId;
        mPath = uriBuilder(path, queryParams);
    }

    public RequestData(String requestId, String path, String[] inlineParams, Map<String, String> querParams) {
        mRequestId = requestId;
        mPath = String.format(mPath, (Object[]) inlineParams);
        mPath = uriBuilder(path, querParams);
    }

    public String getPath() {
        return mPath;
    }

    public String getRequestId() {
        return mRequestId;
    }

    public void setRequestBasePath(String basePath) {
        mBasePath = basePath;
        mPath = basePath + "/" + mPath;
    }

    private String uriBuilder(String path, Map<String, String> queryParams) {
        Uri.Builder builder = new Uri.Builder().path(path);
        for (String key : queryParams.keySet()) {
            builder.appendQueryParameter(key, queryParams.get(key));
        }
        return builder.toString();
    }
}
