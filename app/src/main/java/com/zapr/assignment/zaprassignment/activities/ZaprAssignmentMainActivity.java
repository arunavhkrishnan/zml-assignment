package com.zapr.assignment.zaprassignment.activities;


import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.SearchView;

import com.zapr.assignment.zaprassignment.R;
import com.zapr.assignment.zaprassignment.adapters.SearchListAdapter;

public class ZaprAssignmentMainActivity extends Activity {

    private RecyclerView mSearchList;
    private SearchView mSearchView;
    private SearchListAdapter mSearchListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zapr_assignment_main);
        mSearchView = findViewById(R.id.search_view);
        mSearchView.setOnQueryTextListener(mOnQueryTextListener);
        mSearchList = findViewById(R.id.search_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mSearchList.setLayoutManager(layoutManager);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSearchListAdapter = new SearchListAdapter();
        mSearchList.setAdapter(mSearchListAdapter);
    }

    SearchView.OnQueryTextListener mOnQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            mSearchListAdapter.setSearchQueryString(query);
            return true;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }
    };
}
