package com.zapr.assignment.zaprassignment.core.webservices;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import com.zapr.assignment.zaprassignment.core.http.HttpRequest;
import com.zapr.assignment.zaprassignment.managers.NetworkManager;
import com.zapr.assignment.zaprassignment.core.request.Result;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class WebServiceClient {

    private static final int DEFAULT_TIME_OUT = 30000; //in milliseconds.
    private static final int MESSAGE_ID = 0;

    private HttpClient mHttpClient;

    public WebServiceClient(String host, int port) {
        mHttpClient = new HttpClient(host, port);
    }

    public void sendRequest(HttpRequest request) {
        mHttpClient.sendRequest(request);
    }

    public void cancelRequest(String requestId) {
        mHttpClient.cancelRequest(requestId);
    }

    public void cancelAllRequests() {
        mHttpClient.cancelAllRequests();
    }

    private class HttpClient {

        private Worker mWorker;
        private OkHttpClient mOkHttpClient;
        private String mBasePath;

        public HttpClient(String host, int port) {
            init();
            if (port != -1) {
                mBasePath = host + ":" + Integer.toString(port);
            } else {
                mBasePath = host;
            }
        }

        private void init() {
            mOkHttpClient = new OkHttpClient.Builder().
                    readTimeout(DEFAULT_TIME_OUT, TimeUnit.MILLISECONDS)
                    .connectTimeout(DEFAULT_TIME_OUT, TimeUnit.MILLISECONDS).build();
            mWorker = new Worker(WebServiceClient.class.getSimpleName(), mOkHttpClient);
            mWorker.start();
            mWorker.createHandler();
        }

        private void cancelRequest(String requestId) {
            mWorker.cancelRequest(requestId);
        }

        private void cancelAllRequests() {
            mWorker.cancelAllRequests();
        }

        private void sendRequest(HttpRequest request) {
            request.getRequestData().setRequestBasePath(mBasePath);
            if (NetworkManager.getInstance().isNetworkAvailable()) {
                mWorker.sendRequest(request);
            } else {
                Result result = new Result(Result.STATUS.FAIL, -1, "You are currently offline.");
                request.getResponseHandler().processResult(request.getRequestData(), result);
            }
        }
    }

    private class Worker extends HandlerThread {

        private final Map<String, HttpRequest> mPendingRequests;

        private Handler mHandler;
        private ExecutorService mExecutor = Executors.newCachedThreadPool();
        private OkHttpClient mOkHttpClient;

        private Worker(String name, OkHttpClient okHttpClient) {
            super(name);
            mOkHttpClient = okHttpClient;
            mPendingRequests = Collections.synchronizedMap(new HashMap<String, HttpRequest>());
        }

        private void sendRequest(HttpRequest request) {
            mPendingRequests.put(request.getRequestData().getRequestId(), request);
            mHandler.obtainMessage(MESSAGE_ID, request).sendToTarget();
        }

        private void cancelRequest(String requestId) {
            HttpRequest request = mPendingRequests.get(requestId);
            if (request != null) {
                mPendingRequests.remove(requestId);
                mHandler.removeMessages(MESSAGE_ID, request);
            }
        }

        private void cancelAllRequests() {
            mPendingRequests.clear();
            mHandler.removeCallbacksAndMessages(null);
        }

        private void createHandler() {
            if (mHandler == null) {
                mHandler = new Handler(getLooper()) {
                    @Override
                    public void handleMessage(Message msg) {
                        final HttpRequest request = (HttpRequest) msg.obj;
                        mExecutor.execute(new Runnable() {
                            @Override
                            public void run() {
                                processRequest(request);
                            }
                        });
                    }
                };
            }
        }

        private void processRequest(HttpRequest request) {
            Response response = null;
            try {
                Request okHttpRequest = createOkHttpRequest(request);
                response = mOkHttpClient.newCall(okHttpRequest).execute();
                if (response.isSuccessful()) {
                    int statusCode = response.code();
                    JSONObject responseObject = readResponseObject(response.body().byteStream());
                    Result result = new Result(Result.STATUS.SUCCESS, statusCode, responseObject);
                    request.getResponseHandler().processResult(request.getRequestData(), result);
                } else {
                    handleHttpError(response, null, request);
                }
            } catch (Exception e) {
                handleHttpError(response, null, request);
            } finally {
                if (response != null) {
                    response.close();
                }
            }
        }

        private Request createOkHttpRequest(HttpRequest requestData) {
            Request.Builder builder =  new Request.Builder()
                    .addHeader("Content-Type", "application/json; charset=utf8")
                    .url(requestData.getRequestData().getPath());
            return builder.build();
        }

        private void handleHttpError(Response response, Exception exception, HttpRequest request) {
            int statusCode = -1;
            String errorMsg = "";
            if(exception != null) {
                errorMsg = exception.getLocalizedMessage();
                if (exception instanceof SocketTimeoutException || exception instanceof SocketException) {
                    errorMsg = "Your network connection is currently slow. Please connect to a " +
                            "new network and try again.";
                } else if (exception instanceof JSONException) {
                    errorMsg = "Internal server error";
                }
            } else {
                if(response != null && response.body().byteStream() != null){
                    try {
                        statusCode = response.code();
                        JSONObject responseObject = readResponseObject(response.body().byteStream());
                        Result result = new Result(Result.STATUS.SUCCESS, statusCode, responseObject);
                        request.getResponseHandler().processResult(request.getRequestData(), result);
                        return;
                    } catch (IOException e) {
                        errorMsg = "The App is temporarily unavailable. Please try again soon.";
                    } catch (JSONException e) {
                        errorMsg = "JSON exception, Internal server error: "+ e.getMessage();
                    }
                }
            }

            Result result = new Result(Result.STATUS.FAIL, statusCode, errorMsg);
            request.getResponseHandler().processResult(request.getRequestData(), result);
        }

        private JSONArray readResponseArray(InputStream inputStream) throws IOException, JSONException {
            ByteArrayOutputStream response = new ByteArrayOutputStream();
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = inputStream.read(buffer)) > 0) {
                response.write(buffer, 0, bytesRead);
            }
            response.close();
            return new JSONArray((response.toString("UTF-8")));
        }

        private JSONObject readResponseObject(InputStream inputStream) throws IOException, JSONException {
            ByteArrayOutputStream response = new ByteArrayOutputStream();
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = inputStream.read(buffer)) > 0) {
                response.write(buffer, 0, bytesRead);
            }
            response.close();
            return new JSONObject((response.toString("UTF-8")));
        }
    }
}