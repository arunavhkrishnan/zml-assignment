package com.zapr.assignment.zaprassignment;

import android.app.Application;

import com.zapr.assignment.zaprassignment.managers.NetworkManager;

public class ZAApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        NetworkManager.createInstance(getApplicationContext());
    }
}
