package com.zapr.assignment.zaprassignment.adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zapr.assignment.zaprassignment.R;
import com.zapr.assignment.zaprassignment.datasource.ZADatasource;
import com.zapr.assignment.zaprassignment.datasource.ZAResultCallback;
import com.zapr.assignment.zaprassignment.model.ZACoreItem;
import com.zapr.assignment.zaprassignment.model.ZASearchItem;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SearchListAdapter extends RecyclerView.Adapter {

    private static int DEFAULT_LIMIT = 50;
    private static int NEW_REQUEST_THRESHOLD = 20;

    private List<ZACoreItem> mItems;
    private boolean mRequestProcessing;
    private String mQueryString;

    public SearchListAdapter() {
        mItems = new ArrayList<>();
    }

    public void setSearchQueryString(String queryString) {
        mQueryString = queryString;
        mItems.clear();
        makeRequest();
    }

    private void makeRequest() {
        if (!mRequestProcessing) {
            mRequestProcessing = true;
            ZADatasource.getInstance().getSearchResults(mQueryString, DEFAULT_LIMIT, mResultCallback);
        }
    }

    private ZAResultCallback mResultCallback = new ZAResultCallback() {
        @Override
        public void resultItems(List<ZACoreItem> resultItems) {
            mItems.addAll(resultItems);
            mRequestProcessing = false;
            notifyDataSetChanged();
        }
    };

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View contentView = inflater.inflate(R.layout.list_item, null);
        RecyclerView.ViewHolder viewHolder = new SearchListItemViewHolder(contentView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        SearchListItemViewHolder searchListItemViewHolder = (SearchListItemViewHolder) holder;
        if (mItems.size() > position) {
            ZASearchItem item = (ZASearchItem) mItems.get(position);
            searchListItemViewHolder.mWrapperType.setText(item.getWrapperType());
            searchListItemViewHolder.mKind.setText(item.getKind());
            searchListItemViewHolder.mTrackName.setText(item.getTrackName());
            searchListItemViewHolder.mArtistName.setText(item.getArtistName());
            ImageLoadingTask imageLoadingTask = new ImageLoadingTask(searchListItemViewHolder.mThumbnail, item.getThumbnailUri());
            imageLoadingTask.execute();
            searchListItemViewHolder.mThumbnail.setImageURI(Uri.parse(item.getThumbnailUri()));
            if (mItems.size() - position < NEW_REQUEST_THRESHOLD) {
                makeRequest();
            }
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    private class SearchListItemViewHolder extends RecyclerView.ViewHolder {

        private ImageView mThumbnail;
        private TextView mWrapperType;
        private TextView mKind;
        private TextView mTrackName;
        private TextView mArtistName;

        public SearchListItemViewHolder(View itemView) {
            super(itemView);
            mThumbnail = itemView.findViewById(R.id.content_image);
            mWrapperType = itemView.findViewById(R.id.wrapper_type);
            mKind = itemView.findViewById(R.id.kind);
            mTrackName = itemView.findViewById(R.id.track_name);
            mArtistName = itemView.findViewById(R.id.artist_name);
        }
    }

    private class ImageLoadingTask extends AsyncTask<Void, Void, Bitmap> {

        private ImageView mImageView;
        private String mThumbnailUrl;

        ImageLoadingTask(ImageView imageView, String thumbnailUrl) {
            mImageView = imageView;
            mThumbnailUrl = thumbnailUrl;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            return downloadBitmap(mThumbnailUrl);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap != null) {
                mImageView.setImageBitmap(bitmap);
            }
        }

        private Bitmap downloadBitmap(String thumbnailUrl) {
            Bitmap bitmap =null;
            try{
                URL url = new URL(thumbnailUrl);
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                InputStream is = con.getInputStream();
                bitmap = BitmapFactory.decodeStream(is);
            } catch(Exception e){
                e.printStackTrace();
            }
            return bitmap;
        }
    }
}
