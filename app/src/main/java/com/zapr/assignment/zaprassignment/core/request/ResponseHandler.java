package com.zapr.assignment.zaprassignment.core.request;

public interface ResponseHandler {
    void processResult(RequestData requestData, final Result result);
}
