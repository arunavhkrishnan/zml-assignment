package com.zapr.assignment.zaprassignment.managers;

import android.os.Handler;
import android.os.Looper;

public class ConcurrencyManager {

    private static ConcurrencyManager sConcurrencyManager;
    private Handler mMainThreadHandler;

    private ConcurrencyManager() {
        mMainThreadHandler = new Handler(Looper.getMainLooper());
    }

    public static synchronized ConcurrencyManager getInstance() {
        if (sConcurrencyManager == null) {
            sConcurrencyManager = new ConcurrencyManager();
        }
        return sConcurrencyManager;
    }

    public void runOnMainThread(Runnable runnable) {
        mMainThreadHandler.post(runnable);
    }
}
