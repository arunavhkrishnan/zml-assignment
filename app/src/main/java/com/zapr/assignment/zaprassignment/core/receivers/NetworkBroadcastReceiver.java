package com.zapr.assignment.zaprassignment.core.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public abstract class NetworkBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        onNetworkChange(context);
    }

    protected abstract void onNetworkChange(Context context);
}
